# PostgreSQL SPL Token balances demo

This program demonstrates reading SPL Token balances exported by the Solana [PostgreSQL accounts DB plugin](https://github.com/lijunwangs/solana-accountsdb-plugin-postgres).

## Usage

Account balances of all accounts will be exported for the provided list of tokens (`tokens.json`).

```shell
go build -o balances-demo .
./balances-demo \
  -tokens "tokens.json" \
  -conn-str "host=<ip> user=<user> password=<pass> sslmode=verify-ca dbname=accounts sslcert=client-cert.pem sslkey=client-key.pem sslrootcert=server-ca.pem" \
  > balances-export.csv
```

## Output Format

This program outputs a CSV file with the following columns.

- `pubkey`: Base58-encoded public key
- `slot`: Slot at which balance was last updated
- `mint`: Symbol of token mint (as seen in `tokens.json`)
- `balance`: Uint64 token balance
