package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"flag"
	"log"
	"os"
	"strconv"

	bin "github.com/gagliardetto/binary"
	"github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/programs/token"
	"github.com/jackc/pgx/v4"
)

func init() {
	log.SetOutput(os.Stderr)
}

func main() {
	connStr := flag.String("conn-str", "", "PostgreSQL conn string")
	tokensFile := flag.String("tokens", "tokens.json", "Path to tokens file")
	flag.Parse()

	ctx := context.Background()

	db, err := pgx.Connect(ctx, *connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close(ctx)

	mints, err := readTokensFile(*tokensFile)
	if err != nil {
		log.Fatal("Failed to read tokens file: ", err)
	}
	mintIndex := make(map[solana.PublicKey]mint)
	for _, mint := range mints {
		mintIndex[mint.Mint] = mint
	}

	writer := csv.NewWriter(os.Stdout)
	defer writer.Flush()
	_ = writer.Write([]string{"pubkey", "slot", "mint", "balance"})

	// Source: https://github.com/solana-labs/solana-program-library/blob/master/token/program/src/state.rs
	// Account entries always have length 165.
	// Multisig entries always have length 355.
	// Mint entries always have length 82.
	rows, err := db.Query(ctx, "SELECT pubkey, slot, data FROM account WHERE length(data) = 165")
	if err != nil {
		log.Fatal(err)
	}
	i := 0
	for rows.Next() {
		var pubkeyRaw []byte
		var slot int64
		var data []byte
		if err := rows.Scan(&pubkeyRaw, &slot, &data); err != nil {
			log.Fatal(err)
		}
		pubkey := solana.PublicKeyFromBytes(pubkeyRaw)

		account, err := parseTokenAccount(data)
		if err != nil {
			log.Printf("Failed to parse token account %s: %s", pubkey, err)
			continue
		}

		mint, ok := mintIndex[account.Mint]
		if !ok {
			continue
		}

		err = writer.Write([]string{
			pubkey.String(),
			strconv.FormatInt(slot, 10),
			mint.Symbol,
			strconv.FormatUint(account.Amount, 10),
		})
		if err != nil {
			os.Exit(3) // exit on I/O error
		}

		i++
		if i > 0 && i%1000 == 0 {
			log.Printf("Exported %d accounts", i)
		}
	}
	log.Print("Done")
}

func parseTokenAccount(data []byte) (token.Account, error) {
	decoder := bin.NewBinDecoder(data)
	var acc token.Account
	if err := decoder.Decode(&acc); err != nil {
		return token.Account{}, err
	}
	return acc, nil
}

type mint struct {
	Symbol string
	Mint   solana.PublicKey
}

func readTokensFile(tokensPath string) ([]mint, error) {
	buf, err := os.ReadFile(tokensPath)
	if err != nil {
		return nil, err
	}
	var tokens []mint
	if err := json.Unmarshal(buf, &tokens); err != nil {
		return nil, err
	}
	return tokens, nil
}
